<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'Detect.Camera',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'KTSJ4QKnNCA6eOjoztgN3o6Nl6rgiSPH',
        ],
//        'cache' => [
//            'class' => 'yii\caching\FileCache',
//        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager'   => [
            'enablePrettyUrl'     => true,
            'showScriptName'      => false,
//            'enableStrictParsing' => true,
            /*'rules' => [
                '/logout' => 'site/logout',
                '/login' => 'site/login',
//                '/log' => 'api-video-log/index',
//                '/view/18' => 'api-video-log/view',
//                '/view/<id:\d+>' => 'api-video-log/view',
                '/' => 'api-video-log/index',
            ],*/
            'rules'               => [
                '/' => 'log/index',
                [
                    'pattern' => '<controller>/<action>/<id:\d+>',
                    'route'   => '<controller>/<action>',
                ],
                [
                    'pattern' => '<controller>/<action>',
                    'route'   => '<controller>/<action>',
                ],
                [
                    'pattern' => '<module>/<controller>/<action>/<id:\d+>',
                    'route'   => '<module>/<controller>/<action>',
                ],
                [
                    'pattern' => '<module>/<controller>/<action>',
                    'route'   => '<module>/<controller>/<action>',
                ],
            ],
        ],
    ],
    'params' => $params,
    'modules' => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
        ]
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
