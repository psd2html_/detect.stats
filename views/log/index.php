<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel \app\models\ApiVideoLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Api Video Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="api-video-log-index">

    <?php Pjax::begin(); ?>

    <?php
    $gridColumns = [
//        ['class' => 'yii\grid\SerialColumn'],
        'created_at',
        'id',
        'response_code' => [
            'attribute' => 'response_code',
            'value' => function ($model) {
                /** @var $model \app\models\ApiVideoLog */
                return $model->decoratorResponseCode();
            },
            'format' => 'raw',
        ],
        'email',
        'video_id',
        [
            'class' => \yii\grid\ActionColumn::className(),
            'template'=>'{view}',
            'buttons'=>[
                'view'=>function ($url, $model) {
                    return Html::a(Html::tag('button', 'Смотреть', ['class' => 'btn btn-sm btn-primary']), ['view', 'id' => $model->id]);
                }
            ],
        ]
    ];

    echo \kartik\grid\GridView::widget([
        'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns, // check the configuration for grid columns by clicking button above
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => true, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' =>  [
            [
                'content' => Html::a('Обновить', ['index'], ['class' => 'btn btn-info']),
                'options' => ['class' => 'btn-group mr-2']
            ],
//            '{export}',
            '{toggleData}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => false,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_PRIMARY,
            'heading' => '<h4>Логирование API POST Video</h4>',
        ],
        'persistResize' => false,
        'toggleDataOptions' => ['minCount' => 10],
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
