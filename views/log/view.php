<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \app\models\ApiVideoLog */

$this->title = '#'.$model->id. ' - '.$model->created_at;
$this->params['breadcrumbs'][] = ['label' => 'Api Video Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="api-video-log-view">

    <h1><?= Html::encode($model->created_at) ?></h1>

    <p><?= Html::a('Назад', ['index', 'igor' => 'Kaliningrad'], ['class' => 'btn btn-info'])?></p>

    <h4>Данные POST-запроса от клиента:</h4>
    <p>
        <?php
        echo '<pre>';
        print_r(unserialize($model->data));
        echo '</pre>';
        ?>
    </p>

    <h4>Заголовки HEADERS POST-запроса от клиента:</h4>
    <p>
        <?php
        echo '<pre>';
        print_r(unserialize($model->request_headers));
        echo '</pre>';
        ?>
    </p>

    <h4>Комментарии по логированию:</h4>
    <p>
        <?php
        echo '<pre>';
        print_r(unserialize($model->message));
        echo '</pre>';
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'email',
            'video_id',
            'response_code',
            'created_at',
        ],
    ]) ?>

</div>
