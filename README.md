﻿## Описание проекта
================================

Раздел статистики для проекта Detect.Camera

Адрес на продакшене: http://stat.detect.camera

Требования к серверу и ПО:
--------------------------------

1. Операционная система: CentOS 7
2. Версия языка PHP: 7.1
3. СУБД: MySQL 3.4
4. Веб-сервер: Apache
5. Пакет: git
6. Пакет: composer
7. Пакет: zip

Установка
--------------------------------

Разворачивание проекта из репозитария:

```
cd ../../var/www/stats_public_html
git init
git remote add origin https://fedorov_antonio@bitbucket.org/psd2html_/detect.stats.git
git pull origin master
```

Инициализация окружения для composer и обновление зависимостей composer:

``` 
composer global require "fxp/composer-asset-plugin:*"
composer install
``` 

Установка разрешений на папки:

```
chmod 0777 runtime
chmod 0777 web/assets
```

Миграции отсутствуют.

Обновление на LIVE-сервере:
--------------------------------

```

Разворачивание проекта из репозитария:

```
cd ../../var/www/stats_public_html
git pull origin master
```

Обновление зависимостей composer:

``` 
composer install
``` 

Миграции отсутствуют.