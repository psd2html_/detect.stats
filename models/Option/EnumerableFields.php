<?php
/**
 * Created by PhpStorm.
 * User: antonfedorov
 * Date: 31/07/2019
 * Time: 15:46
 */

namespace app\models\Option;


trait EnumerableFields
{
    /**
     * @param string $attribute
     * @return array|null
     */
    static public function enumItem($attribute) {
        $res = preg_match('/enum\((.*)\)/', self::getTableSchema()->columns[$attribute]->dbType, $matches);
        if (!$res) {
            return null;
        }

        $values = [];
        foreach (explode(',', $matches[1]) as $value) {
            $value = trim($value);
            $value = str_replace('\'', '', $value);
            $values[] = $value;
        }

        return $values;
    }
}