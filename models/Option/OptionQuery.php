<?php
/**
 * Created by PhpStorm.
 * User: antonfedorov
 * Date: 31/07/2019
 * Time: 15:47
 */

namespace app\models\Option;


/**
 * This is the ActiveQuery class for [[Option]].
 *
 * @see Option
 */
class OptionQuery extends \yii\db\ActiveQuery
{
    /**
     * @param string $name
     * @return $this
     */
    public function byName($name) {
        return $this->andWhere(['name' => $name]);
    }

    /**
     * @inheritdoc
     * @return Option[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Option|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}