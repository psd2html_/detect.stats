<?php
/**
 * Created by PhpStorm.
 * User: antonfedorov
 * Date: 31/07/2019
 * Time: 15:43
 */

namespace app\models\Option;

use Yii;

/**
 * This is the model class for table "option".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property string $type
 * @property string $comment
 */
class Option extends \yii\db\ActiveRecord
{
    use EnumerableFields;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'option';
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['comment', 'type', 'value'], 'string'],
            [['type'], 'in', 'range' => self::enumItem('type')],
            [['name'], 'unique'],
        ];
    }


    /** @inheritdoc */
    public function fields()
    {
        $fields = parent::fields();
        unset($fields['id']);
        return $fields;
    }


    static public function staticAttributeLabels()
    {
        return [
            'id'      => 'ID',
            'name'    => Yii::t('common', 'Name'),
            'value'   => Yii::t('common', 'Value'),
            'type'    => 'Type',
            'comment' => Yii::t('common', 'Comment'),
        ];
    }


    /** @inheritdoc */
    public function attributeLabels()
    {
        return self::staticAttributeLabels();
    }


    /**
     * @inheritdoc
     * @return OptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OptionQuery(get_called_class());
    }


    /**
     * @param string $name
     * @return static|null
     */
    public static function findByName($name)
    {
        return static::find()->byName($name)->one();
    }


    /**
     * @param string $name
     * @return string|integer|boolean|null|array
     */
    public static function getValueByName($name)
    {
        $optionRecord = self::findByName($name);
        if (!$optionRecord) return null;
        if (!isset($optionRecord->value)) return null;

        switch ($optionRecord->type) {
            case OptionType::INTEGER:
            case OptionType::UNIX_TIMESTAMP:
                return (int)$optionRecord->value;
            case OptionType::BOOLEAN:
                return filter_var($optionRecord->value, FILTER_VALIDATE_BOOLEAN);
            case OptionType::ARRAY_TO_STRING:
                return unserialize($optionRecord->value);
            default:
                return $optionRecord->value;
        }
    }
}
