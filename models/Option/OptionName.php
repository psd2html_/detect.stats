<?php
/**
 * Created by PhpStorm.
 * User: antonfedorov
 * Date: 31/07/2019
 * Time: 15:45
 */

namespace app\models\Option;


class OptionName
{
    const UA_VIDEOS_STORE_DURATION       = 'userAgent.videos.storeDuration';
    const FE_MY_VIDEOS_SHOW_YOUTUBE_LINK = 'frontend.my-videos.show-youtube-link';
}