<?php
/**
 * Created by PhpStorm.
 * User: antonfedorov
 * Date: 31/07/2019
 * Time: 15:45
 */

namespace app\models\Option;


class OptionType
{
    const INTEGER         = 'INTEGER';
    const STRING          = 'STRING';
    const BOOLEAN         = 'BOOLEAN';
    const UNIX_TIMESTAMP  = 'UNIX_TIMESTAMP';
    const ARRAY_TO_STRING = 'ARRAY';
}