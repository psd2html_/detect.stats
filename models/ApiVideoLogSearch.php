<?php
/**
 * Created by PhpStorm.
 * User: antonfedorov
 * Date: 06/04/2019
 * Time: 22:08
 */

namespace app\models;


use yii\base\Model;
use yii\data\ActiveDataProvider;

class ApiVideoLogSearch extends ApiVideoLog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'response_code', 'id', 'video_id'], 'integer'],
            [['request_headers', 'data', 'created_at'], 'safe'],
            [['email'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApiVideoLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['created_at' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'video_id' => $this->video_id,
            'created_at' => $this->created_at,
            'email' => $this->email,
        ]);

        return $dataProvider;
    }
}