<?php
/**
 * Created by PhpStorm.
 * User: antonfedorov
 * Date: 06/04/2019
 * Time: 22:07
 */

namespace app\models;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%api_video_log}}".
 *
 * @property int $id
 * @property string $email
 * @property string $data
 * @property string $request_headers
 * @property int $response_code
 * @property string $message
 * @property string $created_at
 * @property integer $video_id
 */
class ApiVideoLog extends ActiveRecord
{
    /**
     * Код ответа 102 Processing
     */
    const RESPONSE_CODE_102 = 102;

    /**
     * Код ответа 201 Created
     */
    const RESPONSE_CODE_201 = 201;

    /**
     * Код ответа 400 Bad Request
     */
    const RESPONSE_CODE_400 = 400;


    /**
     * Код ответа 500 Internal Server Error
     */
    const RESPONSE_CODE_500 = 500;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class'              => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value'              => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%api_video_log}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['data', 'required'],
            [['response_code', 'video_id'], 'integer'],
            [['id', 'created_at'], 'safe'],
            [['data', 'request_headers', 'email', 'message'], 'string'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'email'             => 'Email',
            'data'              => 'Лог-данные',
            'request_headers'   => 'Заголовки HTTP-запроса',
            'response_code'     => 'Код ответа',
            'message'           => 'Сообщение для лога',
            'created_at'        => 'Дата добавления',
            'video_id'          => 'ID Видео',
        ];
    }

    /**
     * Сеттер статус ответа
     * @param int $code
     */
    public function setResponseCode(int $code)
    {
        $this->response_code = $code;
        $this->update();
    }

    /**
     * Сеттер сообщение для лога
     * @param $message
     */
    public function setMessage($message)
    {
        $all_message = unserialize($this->message);
        $all_message[] = $message;
        $this->message = serialize($all_message);
        $this->update();
    }


    /**
     * Геттер массив кодов ответов
     * @return array
     */
    public static function getResponseCodeArray()
    {
        return [
            self::RESPONSE_CODE_102 => '102 Processing',
            self::RESPONSE_CODE_201 => '201 Created',
            self::RESPONSE_CODE_400 => '400 Bad Request ',
            self::RESPONSE_CODE_500 => '500 Internal Server Error',
        ];
    }

    /**
     * Геттер значение кода ответа
     * @return mixed
     */
    public function getResponseCodeValue()
    {
        return ArrayHelper::getValue(self::getResponseCodeArray(), $this->response_code);
    }

    /**
     * Декоратор для кода отвтета
     */
    public function decoratorResponseCode()
    {
        switch ($this->response_code) {
            case self::RESPONSE_CODE_201:
                $color = 'success'; break;
            case self::RESPONSE_CODE_102:
            case self::RESPONSE_CODE_400:
            case self::RESPONSE_CODE_500:
                $color = 'danger'; break;
            default: $color = 'warning';
        }
        return Html::tag('span', $this->getResponseCodeValue(), ['class' => 'label label-'.$color]);
    }

}